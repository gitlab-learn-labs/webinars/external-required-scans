# External Required Scan

## Overview

This project identifies a single CI/CD YAML file that includes several scanners avaialable for projects within the realm of the compliance framework. The goal is to identify a number of external scanners and scan results, providing a variety of results to the project that can then be acted upon.

To add new scanners to the pipeline, please open an issue and contribute!  The original scanners are all focused on C/C++ processing as that was the driving use case, but we can expand this to whatever our hearts desire.

## Why Would You Do Such a Thing?

Well, in several environments a GitLab consumer may have desired, or required, scanners outside of the GitLab standard offerings. Why this happens is not ours to judge, but what is ours is the responsibility of showing how the scanner selection doesn't matter as much as the integration and the flow achieved with GitLab Ultimate. 

## Integrations

As new scanners are added, please include them in the list below along with a brief overview of the scanner, links, etc. If you've added this to a cool project where issues are discovered and managed, please include that as well so we can all learn from each other.

It must be noted that in its current form this framework does include the typical GitLab scanners, all relegated to their own stage within the pipeline. These may be removed at a future period, but in the mean time I (Rob) liked the way scan results from both internal and external scans show up in the Vulnerabilty report. Note there is an [issue](https://gitlab.com/rjackson-education/external-required-scan/-/issues/1) regarding how the results for internal and external SAST scans are displayed. 


|Scanner|Reference|Notes|
|----|----|----|
|Semgrep|[Website](https://semgrep.dev/docs/semgrep-ci/overview/)|Overlaps IDs with GitLab IDs. Also using Rob's token for his free account|
|Horus|[Website](https://docs.horusec.io/docs/cli/installation/#installation-via-pipeline)|Doesn't provide output in our JSON format, only provides an artifact. **Requires a shell runner with Docker installed**|